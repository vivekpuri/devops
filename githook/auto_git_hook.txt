On the server create a bare repo using command
 git --bare init

then in hooks folder, create a file with name post-receive and paste the following code

In pm2 authorized files, add id_rsa.pub of local machine

#!/bin/bash
echo "Pushing to server "
while read oldrev newrev refname
do
   branch=$(git rev-parse --symbolic --abbrev-ref $refname)
   if [ "master" == "$branch" ]; then
       echo "in master branch"
       git --work-tree=/apps/node-apps/repo_name1 --git-dir=/apps/node-apps/bare_repo_name checkout $branch -f

  elif [ "dev" == "$branch" ]; then
       echo "in develop branch"
       git --work-tree=/apps/node-apps/repo_name2 --git-dir=/apps/node-apps/bare_repo_name checkout $branch -f

  elif [ "test" == "$branch" ]; then
       echo "in test branch"
       git --work-tree=/apps/node-apps/repo_name3 --git-dir=/apps/node-apps/bare_repo_name checkout $branch -f

  fi
done

chmod 755 post-receive

then on local

Edit the .git/config file
Add pushurl=url of bitbucket
add pushurl=ssh://pm2@IP:/apps/node-apps/bare_repo-name

